#------------------------#
# Configurable variables #
#------------------------#

# Delete files on error or iterruption (does not delete folder targets)
.DELETE_ON_ERROR:

#---------------#
# Final targets #
#---------------#

data: TODO

#-------------#
# Environment #
#-------------#

## Create local Conda environment in "condaenv" folder
condaenv: environment.yml
	hpc/condaenv $<
	hpc/conda-job $@ jupyter lab build

## Run JupyterLab locally or on HPC using qsub
lab: condaenv
	hpc/lab
        
#------------------#
# Utility commands #
#------------------#

# Auto-generated help command (make help)
# Adapted from: https://raw.githubusercontent.com/nestauk/patent_analysis/3beebda/Makefile
help:
	@echo "$$(tput bold)Available rules:$$(tput sgr0)"
	@echo
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| LC_ALL='C' sort --ignore-case \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}' \
	| more $(shell test $(shell uname) = Darwin && echo '--no-init --raw-control-chars')

#------------#
# Data files #
#------------#

data/{data}: 
	mkdir -p $@
	wget -O $@.zip {data}
	cd $(@D); unzip $(@F).zip; rm $(@F).zip

data/models/wav2vec/wav2vec_large.pt:
	mkdir -p $(@D)
	wget -O $@ https://dl.fbaipublicfiles.com/fairseq/wav2vec/wav2vec_large.pt

#---------#
# Package #
#---------#
    
package-linux: bin/wav2vec/wav2vec_f.linux.zip

package-win: bin/wav2vec/wav2vec_f.win.zip

bin/wav2vec/wav2vec_f.linux.zip: bin/wav2vec/wav2vec_f.py data/models/wav2vec/wav2vec_large.pt condaenv
	source activate ./condaenv; \
    cd $(@D); \
    pyinstaller $(<F) \
        --noconfirm \
        --hidden-import pkg_resources.py2_warn \
        --hidden-import fairseq.criterions \
        --add-data '../../$(word 2,$^):.' \
    && zip -r $(@F) dist/wav2vec_f
        
bin/wav2vec/wav2vec_f.win.zip: bin/wav2vec/wav2vec_f.py data/models/wav2vec/wav2vec_large.pt condaenv
	source activate ./condaenv; \
    cd $(@D); \
    pyinstaller $(<F) \
        --noconfirm \
        --hidden-import pkg_resources.py2_warn \
        --hidden-import fairseq.criterions \
        --add-data '../../$(word 2,$^):.' \
    && zip -r $(@F) dist/wav2vec_f
        
