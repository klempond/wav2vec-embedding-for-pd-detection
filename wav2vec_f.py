#!/usr/bin/env python

import torch
from fairseq.models.wav2vec import Wav2VecModel
from scipy.io import wavfile
from scipy import signal
import pandas as pd
import numpy as np
import os
import argparse
import pkgutil
import sys

WAV2VEC_WAV_RATE = 16000

def wav2vec(wav_path, model):
    wav_rate, wav_data = wavfile.read(wav_path)
    original_length = wav_data.shape[0]
    resampled_length = int(original_length / wav_rate * WAV2VEC_WAV_RATE)
    wav_data_16khz = signal.resample(wav_data, resampled_length).reshape(1, -1)
    with torch.no_grad():
        feature_vector = model.feature_extractor(torch.from_numpy(wav_data_16khz).float().to('cpu'))
        context_vector = model.feature_aggregator(feature_vector)
        return context_vector.numpy()[0]    
    
if __name__ == "__main__":
    # Parse command line
    parser = argparse.ArgumentParser()
    parser.add_argument("input", help="Input WAV file path")
    parser.add_argument("output", help="Output feature TSV file path")
    parser.add_argument("--sum", action='store_true', default=False, help="Store sum of each feature instead of storing full positional matrix")
    parser.add_argument("--wav2vec", default=None, help="Custom path to trained wav2vec model (packaged wav2vec_large version used by default)")
    options = parser.parse_args()
    
    if not options.wav2vec:
        d = os.path.dirname(sys.modules['wav2vec_biosignal'].__file__)
        options.wav2vec = os.path.abspath(os.path.join(d, 'wav2vec_large.pt'))
    
    if not os.path.exists(options.wav2vec):
        print('-----')
        print(f'Wav2vec model does not exist: {options.wav2vec}')
        print('Use --wav2vec to provide a valid wav2vec model path')
        sys.exit(1)
    
    cp = torch.load(options.wav2vec, map_location=torch.device('cpu'))
    model = Wav2VecModel.build_model(cp['args'], task=None)
    model.load_state_dict(cp['model'])
    model.eval()
    
    features = wav2vec(options.input, model=model)
    
    columns = [f'feat{i}' for i in range(1, 513)]
    features = pd.DataFrame(features.transpose(), columns=columns)
    if options.sum:
        pd.DataFrame([features.sum()]).to_csv(options.output, sep='\t', index=False)
    else:
        features.to_csv(options.output, sep='\t', index=True)
    print('Saved to:', options.output)    
    
    